#pragma once

#include <opencv2/highgui/highgui.hpp>

class EmotionRecognizer
{
public:
				static EmotionRecognizer* instance();
				static void               removeInstance();

				void diff(cv::Mat& src, cv::Mat& dst, unsigned char& difference);

private:
				static EmotionRecognizer* instance_;

private:
				EmotionRecognizer()  {}
				~EmotionRecognizer() {}
				EmotionRecognizer(const EmotionRecognizer&)            = delete;
				EmotionRecognizer& operator=(const EmotionRecognizer&) = delete;
};