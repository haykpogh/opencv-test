﻿#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "EmotionRecognizer.h"
#include <thread>
#include <iostream>

int main()
{
    ///const std::string path = "C:/Users/dtevany/Documents/Diplomain/OpenCV/MoodPictures/";
    const std::string path = "C:/Users/phayk/Desktop/OpenCV/MoodPictures";

    cv::Mat normalMouth   = cv::imread(path + "Normal/normalMouth.jpg", -1);
    cv::Mat surprisedEyes = cv::imread(path + "Surprised/surprisedEyes.jpg",-1);
    cv::Mat normalEyes    = cv::imread(path + "Normal/normalEyes1.jpg", -1);
    cv::Mat surprisedMouth = cv::imread(path + "Surprised/surprisedMouth.jpg", -1);
    cv::Mat normalIcon    = cv::imread(path + "nIcon.jpg", -1);
    cv::Mat surprisedIcon = cv::imread(path + "sIcon.jpg", -1);

    return 0;
    //EmotionRecognizer* instance = EmotionRecognizer::instance();

    //cv::CascadeClassifier face_cascade;
    //face_cascade.load("haarcascade_frontalface_alt.xml");

    //cv::VideoCapture captureDevice;
    //captureDevice.open(0);

    //cv::Mat captureFrame;
    //cv::Mat grayFrame;

    //std::vector<cv::Rect> faces;

    //while (captureDevice.read(captureFrame) && cv::waitKey(33))
    //{
    //    cvtColor(captureFrame, grayFrame, CV_BGR2GRAY);
    //    equalizeHist(grayFrame, grayFrame);

    //    unsigned char normalE = 0;
    //    unsigned char surprisedE = 0;
    //    unsigned char normalM = 0;
    //    unsigned char surprisedM = 0;

    //    face_cascade.detectMultiScale(grayFrame, faces, 1.1, 3, CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_SCALE_IMAGE, cv::Size(30, 30));
    //    for (int i = 0; i < faces.size(); i++)
    //    {
    //        cv::Point point1(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
    //        cv::Point point2(faces[i].x, faces[i].y);

    //        rectangle(captureFrame, point1, point2, cv::Scalar(255, 0, 0), 2);

    //        cv::Rect faceRec(point1, point2);
    //        cv::Mat cropped = captureFrame(faceRec);
    //        cv::Rect eyesRec(cv::Point(25, cropped.size().height / 5), cv::Point(cropped.size().width - 21, cropped.size().height / 2));
    //        cv::Mat eyes = cropped(eyesRec);
    //        cv::Rect mouthRec(cv::Point(cropped.size().width / 3, cropped.size().height * 0.68), cv::Point(cropped.size().width / 3 * 2, cropped.size().height * 0.99));
    //        cv::Mat mouth = cropped(mouthRec);

    //        cv::Mat grayEyes;
    //        cv::Mat grayMouth;

    //        cvtColor(eyes, grayEyes, CV_BGR2GRAY);
    //        equalizeHist(grayEyes, grayEyes);

    //        cvtColor(mouth, grayMouth, CV_BGR2GRAY);
    //        equalizeHist(grayMouth, grayMouth);

    //        cv::Mat tmpMouth = grayMouth.clone();
    //        cv::Mat tmpEyes  = grayEyes.clone();

    //        tmpEyes.resize(normalEyes.rows);
    //        instance->diff(tmpEyes, normalEyes, normalE);

    //        tmpEyes.resize(surprisedEyes.rows);
    //        instance->diff(tmpEyes, surprisedEyes, surprisedE);

    //        tmpMouth.resize(normalMouth.rows);
    //        instance->diff(tmpMouth, normalMouth, normalM);

    //        tmpMouth.resize(surprisedMouth.rows);
    //        instance->diff(tmpMouth, surprisedMouth, surprisedM);

    //        cv::Rect roiS(cv::Point(captureFrame.cols - surprisedIcon.cols, captureFrame.rows - surprisedIcon.rows), surprisedIcon.size());
    //        cv::Rect roiN(cv::Point(captureFrame.cols - normalIcon.cols, captureFrame.rows - normalIcon.rows), normalIcon.size());
    //        normalE + normalM < surprisedE + surprisedM ? normalIcon.copyTo(captureFrame(roiN)) : surprisedIcon.copyTo(captureFrame(roiS));

    //    }
    //    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    //    cv::imshow("Camera", captureFrame);
    //    EmotionRecognizer::removeInstance();        
    //}
}

