#include "EmotionRecognizer.h"


EmotionRecognizer* EmotionRecognizer::instance_ = nullptr;

EmotionRecognizer* EmotionRecognizer::instance()
{
				if (instance_ == nullptr)
				{
								instance_ = new EmotionRecognizer();
				}
				return instance_;
}

void EmotionRecognizer::removeInstance()
{
				delete instance_;
				instance_ = nullptr;
}

void EmotionRecognizer::diff(cv::Mat& src, cv::Mat& dst, unsigned char& difference)
{
				if (src.channels() != dst.channels() || src.channels() != 1) return;

				auto rows = src.rows;
				if (dst.rows != rows) return;

				int columns = std::min(src.cols, dst.cols);

				if (src.isContinuous()) 
				{
								columns *= rows;
								rows     = 1;
				}

				unsigned long tempDifference = 0;

				for (int i = 0; i < rows; ++i)
				{
								unsigned char* src_row = src.ptr<unsigned char>(i);
								unsigned char* dst_row = dst.ptr<unsigned char>(i);
								for (int j = 0; j < columns; ++j)
								{
												tempDifference += abs(src_row[j] - dst_row[j]);
								}
				}
				difference = tempDifference / (rows * columns);
}

